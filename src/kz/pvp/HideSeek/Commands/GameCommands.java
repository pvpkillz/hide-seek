package kz.pvp.HideSeek.Commands;

import java.util.ArrayList;
import java.util.List;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.ConvertTimings;
import kz.pvp.HideSeek.Utilities.Files;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;




public class GameCommands extends JavaPlugin implements CommandExecutor{
	
	public Main plugin;
	   
	   public GameCommands(Main main) {
		 	this.plugin = main;
	}
	   
	   
@Override
public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
{
  Player p = (Player)sender;
  Location loc = p.getLocation();
  double X = loc.getX();
  double Y = loc.getY();
  double Z = loc.getZ();
  double Yaw = (double)loc.getYaw();
  double P = (double)loc.getPitch();
  List<String> NewList = Files.arenas.getStringList("Maps");
  
  if (cmd.getName().equalsIgnoreCase("hideseek") && ((p.hasPermission("hideseek.*") || (p.hasPermission("hideseek.admin"))))){
	  if (args.length == 0){
		  p.sendMessage(ChatColor.GOLD + "==== " + ChatColor.GRAY + "Hide N Seek Admin Menu" + ChatColor.GOLD + " ====");
		  p.sendMessage(ChatColor.RED + "/hideseek Create/Remove <ArenaName>");
		  p.sendMessage(ChatColor.RED + "/hideseek start <Arena> <seekers/hiders>");
		  p.sendMessage(ChatColor.RED + "/hideseek wait <Arena> seekers");
	  }
	  else if (args.length == 1){
		  if (args[0].equalsIgnoreCase("edit")){
			  if (Main.Editor.contains(p))
				  Main.Editor.remove(p);
			  else
			  Main.Editor.add(p);
		  }
	  }
	  else if (args.length == 2){
		  if (args[0].equalsIgnoreCase("create")){
		  NewList.add(args[1]);
		  Files.arenas.set("Maps",NewList);
		  }
		  else if (args[0].equalsIgnoreCase("remove")){
		  NewList.remove(args[1]);
		  Files.arenas.set("Maps",NewList);
		  }
		  SaveTeleports(p);
	  }
	  else if (args.length == 3){
		  if (args[0].equalsIgnoreCase("start")){
			  if (NewList.toString().toLowerCase().contains(args[1].toLowerCase())){
				  if (args[2].equalsIgnoreCase("seekers")){
					  Files.arenas.set("Map." + args[1] + ".Seekers.X", X);
					  Files.arenas.set("Map." + args[1] + ".Seekers.Y", Y);
					  Files.arenas.set("Map." + args[1] + ".Seekers.Z", Z);
					  Files.arenas.set("Map." + args[1] + ".Seekers.Yaw", Yaw);
					  Files.arenas.set("Map." + args[1] + ".Seekers.Pitch", P);
					  }
				  else if (args[2].equalsIgnoreCase("hiders")){
					  Files.arenas.set("Map." + args[1] + ".Hiders.X", X);
					  Files.arenas.set("Map." + args[1] + ".Hiders.Y", Y);
					  Files.arenas.set("Map." + args[1] + ".Hiders.Z", Z);
					  Files.arenas.set("Map." + args[1] + ".Hiders.Yaw", Yaw);
					  Files.arenas.set("Map." + args[1] + ".Hiders.Pitch", P);
				  }
			  }
		  }
		  else if (args[0].equalsIgnoreCase("wait")){
			  if (NewList.toString().toLowerCase().contains(args[1].toLowerCase())){
				  if (args[2].equalsIgnoreCase("seekers")){
					  Files.arenas.set("Map." + args[1] + ".WaitSeek.X", X);
					  Files.arenas.set("Map." + args[1] + ".WaitSeek.Y", Y);
					  Files.arenas.set("Map." + args[1] + ".WaitSeek.Z", Z);
					  Files.arenas.set("Map." + args[1] + ".WaitSeek.Yaw", Yaw);
					  Files.arenas.set("Map." + args[1] + ".WaitSeek.Pitch", P);
				  }
			  }
		  }
		  else if (args[0].equalsIgnoreCase("disguise")){
			  if (NewList.toString().toLowerCase().contains(args[1].toLowerCase())){
				  List<Integer> List;
				  if (Files.arenas.contains("Map." + args[1] + ".Disguises"))
				  List = Files.arenas.getIntegerList("Map." + args[1] + ".Disguises");
				  else
				  List = new ArrayList<Integer>();
				  
				  List.add(Integer.parseInt(args[2]));
				  Files.arenas.set("Map." + args[1] + ".Disguises", List);
			  }
		  }
		  SaveTeleports(p);
	  }
	  return true;
	}
  	if (cmd.getName().equalsIgnoreCase("hs")){
  		if (args.length == 0){
  			Message.P(p, Message.CurrentTime, true);
  		}
  		else if (args.length == 1){
  			if (Integer.parseInt(args[0]) >= 0){ 
  				Message.P(p, Message.CurrentTimeSet, true);
  				Main.CurrentSeconds = Integer.parseInt(args[0]);
  			}
  			else{
  				Message.P(p, Message.InvalidEntry, true);
  			}
  		}
  		return true;
  	}
  return false;

}
public static void SaveTeleports(Player p){
	 	Files.saveFile(Files.arenas, "arenas.yml");
		p.sendMessage(ChatColor.RED + "Point has been set!");
}
}