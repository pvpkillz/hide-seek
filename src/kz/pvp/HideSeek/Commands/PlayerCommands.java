package kz.pvp.HideSeek.Commands;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Main.Mysql;
import kz.pvp.HideSeek.Utilities.Files;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;




public class PlayerCommands extends JavaPlugin implements CommandExecutor{
	
	public Main plugin;
	   
	   public PlayerCommands(Main main) {
		 	this.plugin = main;
	}
	   
	   
	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args)
	{
	  Player p = (Player)sender;
	  if (cmd.getName().equalsIgnoreCase("gems")){
		  if (args.length == 0){
			  // Person wants to know their gems
			try {
				Message.P(p, Message.Replacer(Message.Title, Message.YourGems, "%title"), false);
				Message.P(p, Message.Replacer("&7" + Message.YourGems + ": &a%gems", "" + Mysql.getPlayerGems(p.getName()), "%gems"), false);
			} catch (SQLException e) {
				e.printStackTrace();
			}
		  }
		  else if (args.length >= 1){
			  // Person wants to check someone else...
			  Player target = Mysql.getPlayerFromSome(args[0]);
			  if (target != null){
				try {
					Message.P(p, Message.Replacer(Message.Title, Message.PlayerGems, "%title"), false);
					Message.P(p, Message.Replacer("&7" + Message.PlayerGems + ": &a%gems", "" + Mysql.getPlayerGems(target.getName()), "%gems"), false);
				} catch (SQLException e) {
					e.printStackTrace();
				}
			  }
			  else{
				  Message.P(p, Message.InvalidPlayer, true);
			  }
		  }
		  
		return true; 	
	  }
	  return false;
	}
}