package kz.pvp.HideSeek.Events;

import kz.pvp.HideSeek.Enums.GameState;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.TeamManagement;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityShootBowEvent;
import org.bukkit.event.entity.ProjectileLaunchEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerBedEnterEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerFishEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.event.entity.EntityMountEvent;


public class BlockEverything extends JavaPlugin implements Listener{
	public static Main plugin;

	
	
	public BlockEverything(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	@EventHandler
	public void PlayerQuit (PlayerQuitEvent e){
	e.setQuitMessage(null);
	}
	@EventHandler
	public void InventoryUse (PlayerDropItemEvent e){
	e.setCancelled(true);
	}
	@EventHandler
	public void InventoryChange (InventoryClickEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void InventoryPickUp (PlayerPickupItemEvent e){
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockBreak (BlockBreakEvent e){
		if (Main.Editor.contains(e.getPlayer()))
		e.setCancelled(false);
		else
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockPlace (BlockPlaceEvent e){
		if (Main.Editor.contains(e.getPlayer()))
		e.setCancelled(false);
		else
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockFishing (PlayerFishEvent e){
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockBedEntry (PlayerBedEnterEvent e){
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockMount (EntityMountEvent e){
		e.setCancelled(true);
	}
	@EventHandler
	public void BlockChat (AsyncPlayerChatEvent e){
		// Chat per team!
		
	}
	@EventHandler
	public void BlockAttackingAnyDmg (EntityDamageByEntityEvent e){
		if (e.getEntity() instanceof Player && e.getDamager() instanceof Player){
			Player p = (Player) e.getEntity();
		if (TeamManagement.isHider(p) && TeamManagement.isHider((Player) e.getDamager())){
			e.setCancelled(true);
			}
		if (TeamManagement.isSeeker(p) && TeamManagement.isSeeker((Player) e.getDamager())){
			e.setCancelled(true);
			}
		if (Main.game == GameState.PREGAME || Main.game == GameState.TIMER)
			e.setCancelled(true);
		}
	}
	
	
	@EventHandler
	public void Block (EntityShootBowEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void OnPlayerShoot (ProjectileLaunchEvent e){
		e.setCancelled(true);
	}
	
	@EventHandler
	public void OnItemInteractEvent (PlayerInteractEvent e){
	}
	
	
	
}
