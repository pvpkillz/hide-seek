package kz.pvp.HideSeek.Events;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.TeamManagement;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.plugin.java.JavaPlugin;



public class DeathListener extends JavaPlugin implements Listener{
	public static Main plugin;
	
	public DeathListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	
	
	@EventHandler
	public void OnDmgEvent (EntityDamageEvent e){
		if (e.getCause() == DamageCause.SUFFOCATION || e.getCause() == DamageCause.DROWNING || e.getCause() == DamageCause.FALL || e.getCause() == DamageCause.LAVA)
			e.setCancelled(true);	
	}
	
	
	@EventHandler
	public void OnDmgEvent (PlayerDeathEvent e){
		e.setDeathMessage(null);
		if (e.getEntity() instanceof Player){
			Player def = (Player) e.getEntity();
			def.setHealth(20.0);
			def.setFoodLevel(20);
			e.getDrops().clear();
			if (def.getKiller() instanceof Player){
				if (TeamManagement.isHider(def)){
					TeamManagement.EliminatedHider(def, def.getKiller());
				}
				else if (TeamManagement.isSeeker(def)){
					TeamManagement.respawnSeeker(def, 10);
				}
			}
		}
	}
	
	
}
