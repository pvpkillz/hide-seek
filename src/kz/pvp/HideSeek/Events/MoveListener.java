package kz.pvp.HideSeek.Events;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.DisguiseData;
import kz.pvp.HideSeek.Methods.Disguises;
import kz.pvp.HideSeek.Methods.TeamManagement;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerMoveEvent;
import org.bukkit.plugin.java.JavaPlugin;


public class MoveListener extends JavaPlugin implements Listener{
	public static Main plugin;
	
	public MoveListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	@EventHandler
	public void OnMove (PlayerMoveEvent e){
		Player p = e.getPlayer();
		
		double fx = e.getFrom().getBlockX();
		double fz = e.getFrom().getBlockZ();
		
		double tx = e.getTo().getBlockX();
		double tz = e.getTo().getBlockZ();
		
		if (fx != tx || fz != tz){
			if (DisguiseData.HiddenBlocks.containsKey(p.getName())){
				if (TeamManagement.isHider(p)){
					Disguises.disguiseAsB(p, DisguiseData.getBlockDisguise(p), false);
				}
			}
		}
		else if (DisguiseData.isDisguised(p) && !DisguiseData.HiddenBlocks.containsKey(p.getName())){
			if (Main.CountdownTimer.containsKey(p.getName())){
				Main.CountdownTimer.put(p.getName(), 6);
			}
		}
		
		
		
		
		
		
		
	}
	
	
}
