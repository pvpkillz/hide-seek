package kz.pvp.HideSeek.Events;

import java.sql.SQLException;
import java.util.HashMap;

import kz.pvp.HideSeek.Enums.GameState;
import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.ConvertTimings;
import kz.pvp.HideSeek.Methods.TeamManagement;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerKickEvent;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.ScoreboardManager;


public class JoinListener extends JavaPlugin implements Listener{
	public static Main plugin;
	public static HashMap<String, Objective> Objectives = new HashMap<String, Objective>();
	public static HashMap<String, Scoreboard> Boards = new HashMap<String, Scoreboard>();
	
	
	static Scoreboard board;
	static ScoreboardManager manager;
	public static Objective objective;
	
	
	public JoinListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
	
	@EventHandler
	public void OnServerJoin (PlayerJoinEvent e) throws SQLException{
		final Player p = e.getPlayer();
		if (Main.game == GameState.PREGAME){
			if (p.hasPermission("hideseek.announcejoin"))
				e.setJoinMessage(Message.Replacer(Message.JoinedServer, "&9" + p.getName(), "%player"));
			else
				e.setJoinMessage(Message.Replacer(Message.JoinedServer, "&7" + p.getName(), "%player"));
		}
		else
			e.setJoinMessage(null);
		
		
		JoinedServer(p);
	}
	
	
	


	

	@EventHandler
	public void OnPlayerJoin (PlayerLoginEvent e){
		Player p = e.getPlayer();
		if (Main.game != GameState.PREGAME){
			// Player has joined the game, after it began.
			if (p.hasPermission("hideseek.spectate")){
				// User was not in the game and joined the server.
				e.allow();
			}
			else{
				e.disallow(PlayerLoginEvent.Result.KICK_OTHER, Message.GameInProgress);
			}
		}
	}
	 public static void JoinedServer(Player p) {
			
			p.teleport(p.getWorld().getSpawnLocation());
			
			if (Main.game == GameState.PREGAME){
				if (Main.CurrentSeconds >= 90 && plugin.getServer().getOnlinePlayers().length >= HideSeek.MinStart){
					BringDownTimer(90);
				}
				else if (Main.CurrentSeconds >= 30 && plugin.getServer().getOnlinePlayers().length >= HideSeek.QuickStart){
					BringDownTimer(30);
				}
				
				
				
				
				p.getInventory().clear();
				p.getInventory().setChestplate(null);
				p.getInventory().setLeggings(null);
				p.getInventory().setBoots(null);
				p.getInventory().setHelmet(null);
				p.setExp(0);
				p.setLevel(0);
				
				ChatColor Color = ChatColor.BLUE;
				
				
				p.setPlayerListName(Color + p.getName());
				p.setDisplayName(Color + p.getName());
			}
			else
				HideSeek.checkStatus();
	}
	private static void BringDownTimer(int WaitTime) {
		Main.CurrentSeconds = WaitTime;
		
		Message.G(Message.GameBegins, false);
		Message.G(Message.MapPlayed, false);
		Message.G(Message.PlayersOn, false);
		 
	}
	public static void ScoreBoard(Player player){
		if ((!Boards.containsKey(player.getName())) && (!Objectives.containsKey(player.getName()))){
		manager = Bukkit.getScoreboardManager();
		board = manager.getNewScoreboard();
		objective = board.registerNewObjective("test", "dummy");
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);

		if (!Objectives.containsKey(player.getName()))
		Objectives.put(player.getName(), objective);

		if (!Boards.containsKey(player.getName()))
		Boards.put(player.getName(), board);
		}
		
		objective = Objectives.get(player.getName());
		board = Boards.get(player.getName());
		
		
		objective.setDisplayName(ChatColor.GOLD  + ConvertTimings.getTime(Main.CurrentSeconds));
		GiveStat(player, objective, TeamManagement.getHidersSize(), "Hiders", ChatColor.LIGHT_PURPLE);
		GiveStat(player, objective, TeamManagement.getSeekersSize(), "Seekers", ChatColor.LIGHT_PURPLE);
		
		player.setScoreboard(board);
	}
	public static void GiveStat(Player player, Objective objective, Integer integer, String string, ChatColor COLOR) {
		  Score score = objective.getScore(Bukkit.getOfflinePlayer(COLOR + string)); //Get a fake offline player
		  score.setScore(integer); //Integer only!
	}
	@EventHandler
	public void onKick (PlayerKickEvent e){
		e.setLeaveMessage(null);
		Player p = e.getPlayer();
		Main.Editor.remove(p);
		
		TeamManagement.LeaveTeams(p);
		
		if (Main.game == GameState.GAME){
			HideSeek.checkStatus();
		}
		
	}
	
	@EventHandler
	public void OnServerQuit (PlayerQuitEvent e){
		e.setQuitMessage(null);
		Player p = e.getPlayer();
		Main.Editor.remove(p);
		
		TeamManagement.LeaveTeams(p);
		
		if (Main.game == GameState.GAME){
			HideSeek.checkStatus();
		}
		
		
		
	}
	
	
}
