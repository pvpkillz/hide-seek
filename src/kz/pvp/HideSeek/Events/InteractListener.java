package kz.pvp.HideSeek.Events;

import java.util.Map.Entry;
import java.util.Set;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.DisguiseData;
import kz.pvp.HideSeek.Methods.Disguises;
import kz.pvp.HideSeek.Methods.TeamManagement;
import kz.pvp.HideSeek.Utilities.Files;
import me.confuserr.banmanager.BmAPI;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.block.SignChangeEvent;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.java.JavaPlugin;

import ru.tehkode.permissions.PermissionUser;
import ru.tehkode.permissions.bukkit.PermissionsEx;



public class InteractListener extends JavaPlugin implements Listener{
	public static Main plugin;
	
	public InteractListener(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
		}
	
	
	@EventHandler
	public void onChat (AsyncPlayerChatEvent e){
		if (!e.isCancelled() || !BmAPI.isMuted(e.getPlayer().getName())){
		Player p = e.getPlayer();
		PermissionUser user = PermissionsEx.getUser(e.getPlayer());
		String prefx = user.getPrefix();
		
		String msg = "";
		
		if (e.getPlayer().hasPermission("chat.color"))
		msg = ChatColor.translateAlternateColorCodes('&', e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1));
		else
		msg = e.getMessage().substring(0,1).toUpperCase() + e.getMessage().substring(1);
		
		
		String prefix = ChatColor.translateAlternateColorCodes('&', prefx + " ");
		e.setFormat(prefix + ChatColor.GRAY + e.getPlayer().getDisplayName() + ChatColor.GRAY + ": " + msg);		
		}
	}
	
	
	@EventHandler
	public void OnIntract (PlayerInteractEvent e){
		Player p = e.getPlayer();
		Block CB = e.getClickedBlock();
		if (e.getAction() == Action.LEFT_CLICK_BLOCK || e.getAction() == Action.RIGHT_CLICK_BLOCK){
			for (Entry<String, Location> HiddenEntry : DisguiseData.HiddenBlocks.entrySet()){
				if (HiddenEntry.getValue() != null){
					if (CB.getLocation().getBlockX() == HiddenEntry.getValue().getBlockX() && 
							CB.getLocation().getBlockY() == HiddenEntry.getValue().getBlockY() && 
								CB.getLocation().getBlockZ() == HiddenEntry.getValue().getBlockZ()){
						Player hider = null;
						hider = plugin.getServer().getPlayerExact(HiddenEntry.getKey());
						if (TeamManagement.isHider(p) && TeamManagement.isHider(hider))
							e.setCancelled(true);
						else
							Disguises.disguiseAsB(hider, DisguiseData.getBlockDisguise(hider), false);
						break;
					}
				}
			}
		}
	}
	
	@EventHandler
	public void onSignPlace (SignChangeEvent e){
		Player p = e.getPlayer();
		if (p.hasPermission("hideseek.placestatsigns")){
			String[] lines = e.getLines();
			FileConfiguration var = Files.arenas;
			if (lines[0].toLowerCase().contains("hideseek")){
				if (lines[1].toLowerCase().contains("players")){
					int ID = 1;
					if (!var.contains("signs.players")){
						SaveLocation("signs.players." + ID, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
					else{
						Set<String> list = var.getConfigurationSection("signs.players").getKeys(false);
						SaveLocation("signs.players." + list.size() + 1, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
				}
				else if (lines[1].toLowerCase().contains("map")){
					int ID = 1;
					if (!var.contains("signs.map")){
						SaveLocation("signs.map." + ID, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
					else{
						Set<String> list = var.getConfigurationSection("signs.map").getKeys(false);
						SaveLocation("signs.map." + list.size() + 1, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
				}
				else if (lines[1].toLowerCase().contains("start")){
					int ID = 1;
					if (!var.contains("signs.start")){
						SaveLocation("signs.start." + ID, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
					else{
						Set<String> list = var.getConfigurationSection("signs.start").getKeys(false);
						SaveLocation("signs.start." + list.size() + 1, e.getBlock().getWorld(), e.getBlock().getX(), e.getBlock().getY(), e.getBlock().getZ(), 0, 0, p);
					}
				}
				
				
				
			}
			
		}
		
		
		
		
		
		
	}
	
	
	
	private void SaveLocation(String string, World world, double x, double y, double z, double p, double yaw, Player pl) {
			Files.signs.set(string +  ".X", x);
			Files.signs.set(string +  ".Y", y);
			Files.signs.set(string +  ".Z", z);
			Files.signs.set(string +  ".World", world.getName());
			if (p != 0)
				Files.signs.set(string +  ".Pitch", p);
			if (yaw != 0)
				Files.signs.set(string +  ".Yaw", yaw);
	 	  
	 	 SaveSigns(pl);
	}
	public static void SaveSigns(Player p){
	 	Files.saveFile(Files.signs, "signs.yml");
	 }
	
}
