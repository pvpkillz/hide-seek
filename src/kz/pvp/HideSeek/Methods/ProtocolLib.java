package kz.pvp.HideSeek.Methods;

import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;


import org.bukkit.Bukkit;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.inventory.ItemStack;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.Packets;
import com.comphenix.protocol.ProtocolLibrary;
import com.comphenix.protocol.events.ConnectionSide;
import com.comphenix.protocol.events.ListenerPriority;
import com.comphenix.protocol.events.PacketAdapter;
import com.comphenix.protocol.events.PacketEvent;
import com.comphenix.protocol.utility.MinecraftReflection;
import com.comphenix.protocol.wrappers.nbt.NbtCompound;
import com.comphenix.protocol.wrappers.nbt.NbtFactory;

public class ProtocolLib extends JavaPlugin{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public ProtocolLib (Main mainclass){
		plugin = mainclass;
		setupProtocolLib();
	}

	private void setupProtocolLib() {
		EnableProtocolLibFunctions();
	}
	
	// We enable ProtocolLib's API for use with fake enchanted items
    private void EnableProtocolLibFunctions() {
		if (Main.ProtocolLibEnable){
			ProtocolLibrary.getProtocolManager().addPacketListener(new PacketAdapter(
					plugin, ConnectionSide.SERVER_SIDE, ListenerPriority.HIGH, 
					Packets.Server.SET_SLOT, Packets.Server.WINDOW_ITEMS) {
				@Override
				public void onPacketSending(PacketEvent event) {
					if (event.getPacketID() == Packets.Server.SET_SLOT) {
						addGlow(new ItemStack[] { event.getPacket().getItemModifier().read(0) });
					} else {
						addGlow(event.getPacket().getItemArrayModifier().read(0));
					}
				}
			});
		}
	}
	public static void addGlow(ItemStack[] selector) {
		if (Main.ProtocolLibEnable){
			for (ItemStack stack : selector) {
				if (stack != null) {
					// Only update those stacks that have our flag enchantment
					if (stack.getEnchantmentLevel(Enchantment.SILK_TOUCH) == 32) {
						NbtCompound compound = (NbtCompound) NbtFactory.fromItemTag(stack);
						compound.put(NbtFactory.ofList("ench"));
					}
				}
			}
		}
	}

	public static ItemStack setGlowing(ItemStack is) {
		is = MinecraftReflection.getBukkitItemStack(is);
		is.addUnsafeEnchantment(Enchantment.SILK_TOUCH, 32);
		return is;
	}
}
