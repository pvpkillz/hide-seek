package kz.pvp.HideSeek.Methods;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Main.Mysql;
import kz.pvp.HideSeek.Timers.FreezeTimer;
import kz.pvp.HideSeek.Utilities.Files;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.ChatColor;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitTask;

public class TeamManagement {
	public static ArrayList<Player> Hiders = new ArrayList<Player>();
	public static ArrayList<Player> Seekers = new ArrayList<Player>();
	
	
	
	
	public static Main plugin;
	public TeamManagement(Main mainclass) {
		plugin = mainclass;
	}


	public static void joinHiders(final Player p){
		p.setDisplayName(ChatColor.BLUE + p.getName() + ChatColor.GRAY);
		
		final Location HidersSpawn = FileManagement.GetLocOfString("Map." + Main.Map + ".Hiders", Files.arenas);
		
		ItemStack is = DisguiseData.AvailDisguises.get(ConvertTimings.randomInt(0, DisguiseData.AvailDisguises.size() - 1));		
		DisguiseData.PlayerDisguises.put(p.getName(), is);
		Disguises.disguiseAsB(p, is, false);
		
		
		
		
		String block = Message.CleanCapitalize(is.getType().toString());
		Message.P(p,  Message.Replacer(Message.HiddenAs, block, "%block"), true);			
		
		
		
		LeaveTeams(p);
		Hiders.add(p);
		p.teleport(HidersSpawn);
		
		
		// Add to arrays and run timers needed
		Main.CountdownTimer.put(p.getName(), 6);
		
		BukkitTask Freeze = new FreezeTimer(plugin, p).runTaskTimer(plugin, 0L, 20L);
		FreezeTimer.Timer = Freeze;
		Main.Tasks.put(p.getName(), Freeze);
	}
	
	
	public static void joinSeekers(final Player p, int Length){
		Disguises.unDisguise(p);// Incase the seeker is disguised... The checks are made in that method.
		
		/*
		* We want to set the appropriate armor.
		*/
		ClearInventory(p);
		Disguises.unDisguise(p);
		
		
		LeaveTeams(p);
		Seekers.add(p);
		respawnSeeker(p, Length);
		
		p.setDisplayName(ChatColor.RED + p.getName() + ChatColor.GRAY);
		
	}
	

	
	public static void LeaveTeams(Player p) {
		if (Hiders.contains(p))
		Hiders.remove(p);
		if (Seekers.contains(p))
		Seekers.remove(p);
	}



	public static void respawnSeeker(final Player p, int Length){
		Disguises.unDisguise(p);// Incase the seeker is disguised... The checks are made in that method.
		final Location SeekerSpawn = FileManagement.GetLocOfString("Map." + Main.Map + ".Seekers", Files.arenas);
		Location SeekerWait = FileManagement.GetLocOfString("Map." + Main.Map + ".WaitSeek", Files.arenas);
		p.teleport(SeekerWait);
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				p.teleport(SeekerSpawn);
				setupArmorNeeded(p);
			}
		}, 20L * Length);
		
		
	}
	
	
	
	
	
	
	private static void setupArmorNeeded(Player p) {
		if (TeamManagement.isSeeker(p)){
			p.getInventory().setHelmet(new ItemStack (Material.IRON_HELMET, 1));
			p.getInventory().setChestplate(new ItemStack (Material.IRON_CHESTPLATE, 1));
			p.getInventory().setLeggings(new ItemStack (Material.IRON_LEGGINGS, 1));
			p.getInventory().setBoots(new ItemStack (Material.IRON_BOOTS, 1));
			p.getInventory().addItem(new ItemStack(Material.IRON_SWORD, 1));
		}
	}
	
	public static void ClearInventory(Player p) {
		p.getInventory().clear();
		p.getInventory().setHelmet(null);
		p.getInventory().setChestplate(null);
		p.getInventory().setLeggings(null);
		p.getInventory().setBoots(null);
	}
	
	
	
	public static boolean isHider(Player p){
		return Hiders.contains(p);
	}
	public static boolean isSeeker(Player p){
		return Seekers.contains(p);
	}


	public static Integer getSeekersSize() {
		return Seekers.size();
	}


	public static Integer getHidersSize() {
		return Hiders.size();
	}


	public static void EliminatedHider(Player def, Player killer) {
		TeamManagement.joinSeekers(def, 10);
		
		Message.G(Message.Replacer(Message.HiderEliminated, def.getName(), "%hider"), true);
		Message.G(Message.HidersRemain, true);
		Message.P(killer, Message.Replacer(Message.YouEliminated, def.getName(), "%hider"), true);
		try {
			Mysql.modifyUserGems(def.getName(), (int)(Math.floor((HideSeek.EndGameTimer - Main.CurrentSeconds))/120));
			Mysql.modifyUserGems(killer.getName(), (int)(Math.ceil((Main.CurrentSeconds)/300)));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		HideSeek.checkStatus();
		
	}
	
	
	
	
	
	
	
	
	
	
	
}
