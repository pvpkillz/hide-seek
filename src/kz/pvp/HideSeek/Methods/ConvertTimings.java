package kz.pvp.HideSeek.Methods;

import java.util.Random;
import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

public class ConvertTimings extends JavaPlugin{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	
	public ConvertTimings (Main mainclass){
		plugin = mainclass;
	}
	
	public static String convertTime (int seconds){
		String timeString = "";
		double time = 0.0;
		if (seconds < 60){
			time = (double) seconds;
			if (time <= 1)
				timeString = Message.Second;
			else
				timeString = Message.Seconds;
		}
		else if (seconds < 60 * 60){
			time = (double) seconds/60;
			if (time < 2)
				timeString = Message.Minute;
			else
				timeString = Message.Minutes;
		}
		else if (seconds <= 60 * 60 * 24){
			time = (double) seconds/(60 * 60);
			if (time < 2)
				timeString = Message.Hour;
			else
				timeString = Message.Hours;
		}
		else if (seconds > 60 * 60 * 24){
			time = (double) seconds/(60 * 60 * 24);
			if (time < 2)
				timeString = Message.Day;
			else
				timeString = Message.Days;
		}
		String number = "";
		
		if ((int) time == time)
		number = "" + (int) time;
		else{
		number = "" + time;
			if ((double)seconds/60 == time){//Minutes
				double extraTime = 0.0;
				extraTime = 60 * (time - Math.floor(time));
				number = "" + (int)Math.floor(time);
				if ((int)extraTime != 0)
				timeString = timeString + " and " + convertTime((int)extraTime);
			}
			if ((double)seconds/(60*60) == time){//Hours
				double extraTime = 0.0;
				extraTime = (60* 60) * (time - Math.floor(time));
				number = "" + (int)Math.floor(time);
				if ((int)extraTime != 0)
				timeString = timeString + ", " + convertTime((int)extraTime);
			}
		}
		
		return  number + " " + timeString;
	}
	public static String getTime (int seconds){
		
		double time = 0.0;
		double Seconds = 0.0;
		double Minutes = 0.0;
		double Hours = 0.0;
		if (seconds <= 60){
			time = (double) seconds;
			Seconds = seconds;
		}
		else if (seconds <= 60 * 60){
			time = (double) seconds/60;
			Minutes = Math.floor(time);
			Seconds = seconds - (Math.floor(time) * 60);
		}
		else if (seconds <= 60 * 60 * 24){
			time = (double) seconds/(60 * 60);
			Minutes = Math.floor(time);
			Seconds = seconds - (Math.floor(time) * 60);
			Hours = seconds - (Math.floor(Minutes) * 60 * 60);
		}
		
		
		String TimeLook = (int) Hours  + ":" + (int) Minutes + ":" + (int) Seconds;
		if (Hours >= 1)
			TimeLook =(int) Hours  + ":" + (int) Minutes + ":" + (int) Seconds;
		else if (Minutes >= 1)
			TimeLook = (int) Minutes + ":" + (int) Seconds;
		else if (Seconds >= 1)
			TimeLook = "" + Seconds;
			
		
		return  TimeLook;
	}
	public static int randomInt(int Low, int Max){
		Random r = new Random();
		int Random = Low;
		
		if(Low != Max)
		Random = r.nextInt(Max-Low) + Low;
		
		
		return Random;
	}
	
}
