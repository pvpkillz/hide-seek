package kz.pvp.HideSeek.Methods;

import java.security.spec.PKCS8EncodedKeySpec;

import kz.pvp.HideSeek.Main.Main;


import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

public class FakeShow {

	public static Main plugin;
	public FakeShow(Main mainclass) {
		plugin = mainclass;
	}

	
	@SuppressWarnings("deprecation")
	public static void Visibility (Player p, boolean Show){
		if (Show){
			ItemStack is = DisguiseData.getBlockDisguise(p);
			
			int x = p.getLocation().getBlockX();
			int y = p.getLocation().getBlockY();
			int z = p.getLocation().getBlockZ();
			Location loc = new Location (p.getWorld(), x, y, z);
			for (Player pl : plugin.getServer().getOnlinePlayers()){
				if (pl != p){
					pl.hidePlayer(p);
					pl.sendBlockChange(loc, is.getTypeId(), (byte) is.getDurability());
				}
			}
			DisguiseData.HiddenBlocks.put(p.getName(), loc);
		}
		else{
			if (DisguiseData.HiddenBlocks.containsKey(p.getName())){
				for (Player pl : plugin.getServer().getOnlinePlayers()){
						pl.sendBlockChange(DisguiseData.HiddenBlocks.get(p.getName()), 0, (byte) 0);
				}
				DisguiseData.HiddenBlocks.remove(p.getName());
			}
			if (Main.CountdownTimer.containsKey(p.getName())){
				Main.CountdownTimer.put(p.getName(), 6);
			}
		}
	}

}
