package kz.pvp.HideSeek.Methods;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Files;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import pgDev.bukkit.DisguiseCraft.disguise.Disguise;

public class DisguiseData {
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	
	
	
	public static ArrayList<ItemStack> AvailDisguises = new ArrayList<ItemStack>();//Possible disguises...
	
	
	
	public static HashMap<String, ItemStack> PlayerDisguises = new HashMap<String, ItemStack>();//Disguise Info here...
	public static HashMap<String, Location> HiddenBlocks = new HashMap<String, Location>();
	
	
	
	@SuppressWarnings("deprecation")
	public DisguiseData (Main mainclass){
		List<String> Disguises = Files.arenas.getStringList("Map." + Main.Map + ".Disguises");
		for (String disguise : Disguises){
			int BlockID = 0;
			short BlockData = 0;
			
			if (disguise.contains(":")){
				String[] Disguise = disguise.split(":");
				BlockID = Integer.parseInt(Disguise[0]);
				BlockData = (short) Integer.parseInt(Disguise[1]);
			}
			else{
				BlockID = Integer.parseInt(disguise);
				BlockData = 1;
			}
			ItemStack is = new ItemStack (Material.getMaterial(BlockID), 1, BlockData);
			AvailDisguises.add(is);
		}
		
		plugin = mainclass;
	}

	public static ItemStack getBlockDisguise(Player p) {
		return PlayerDisguises.get(p.getName());
	}
	
	public static Disguise getDisguise(Player p) {
		Disguise disguise;
		if (Disguises.dcAPI.getDisguise(p) != null)
			disguise = null;
		else
			disguise = Disguises.dcAPI.getDisguise(p);
		return disguise;
	}
	
	
	public static LinkedList<String> getDisguiseData(Player p){
		LinkedList<String> data = new LinkedList<String>();
		if (getDisguise(p) != null)
			data = getDisguise(p).data;
		return data;
	}

	public static boolean isDisguised(Player p) {
		return Disguises.dcAPI.isDisguised(p);
	}

	public static boolean isFrozenDisguised(Player p) {
		return HiddenBlocks.containsKey(p.getName());
	}
	
	
	
	
	
	
	
	
	
	
	
}
