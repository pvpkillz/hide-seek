package kz.pvp.HideSeek.Methods;

import kz.pvp.HideSeek.Main.Main;
import org.bukkit.Location;
import org.bukkit.configuration.file.FileConfiguration;

public class FileManagement {
	public static Main plugin;
	
	
	
	
	
	public FileManagement(Main mainclass) {
		plugin = mainclass;
		
	}
	
	public static Location GetLocOfString(String string, FileConfiguration file) {
		double x = (double) file.get(string + ".X");
		double y = (double) file.get(string + ".Y");
		double z = (double) file.get(string + ".Z");
		
		Location loc = new Location(plugin.getServer().getWorlds().get(0),x,y,z);
		
		if (file.get(string + ".Yaw") != null){
			double yaw = (double) file.get(string + ".Yaw");
			loc.setYaw((float)yaw);
		}
		if (file.get(string + ".Pitch") != null){
			double pitch = (double) file.get(string + ".Pitch");
			loc.setPitch((float)pitch);
		}
		return loc;
	}
}
