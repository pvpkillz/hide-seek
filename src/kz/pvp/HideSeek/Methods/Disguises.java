package kz.pvp.HideSeek.Methods;

import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import pgDev.bukkit.DisguiseCraft.DisguiseCraft;
import pgDev.bukkit.DisguiseCraft.api.DisguiseCraftAPI;
import pgDev.bukkit.DisguiseCraft.disguise.Disguise;
import pgDev.bukkit.DisguiseCraft.disguise.DisguiseType;

public class Disguises {
	
	public static Main plugin;
	static Logger log = Bukkit.getLogger();

	public Disguises (Main mainclass){
		plugin = mainclass;
		setupDisguiseCraft();
	}
	
	
	public static DisguiseCraftAPI dcAPI;
	public void setupDisguiseCraft() {
		dcAPI = DisguiseCraft.getAPI();
	}
	
	// Disguise as an Entity
	public static void disguiseAsE(Player p, EntityType entity) {
		if (Main.DisguiseCraftEnable){
			Disguise disguise = new Disguise(dcAPI.newEntityID(), getDisguiseType(entity));
			
			if (!dcAPI.isDisguised(p))
				dcAPI.disguisePlayer(p, disguise);
			else
				dcAPI.changePlayerDisguise(p, disguise);
		}
	}
	
	// Disguise as a block
	@SuppressWarnings("deprecation")
	public static void disguiseAsB(Player p, ItemStack is, boolean Freeze) {
		if (Main.DisguiseCraftEnable){
			Disguise Disguise = null;
			if (Freeze == true){
				Disguise = new Disguise(dcAPI.newEntityID(), "blockID:0", DisguiseType.FallingBlock);
				Message.P(p, Message.Replacer(Message.SolidBlock, Message.CleanCapitalize(is.getType().toString()), "%disguise"), false);				
				Disguise.addSingleData("blocklock");
				// We show everyone that there is a REAL block there...
				dcAPI.changePlayerDisguise(p, Disguise);
				FakeShow.Visibility(p, true);
			}
			else{
				Disguise = new Disguise(dcAPI.newEntityID(), "blockID:" + is.getTypeId(), DisguiseType.FallingBlock);
				FakeShow.Visibility(p, false);
				dcAPI.disguisePlayer(p, Disguise);
			}
		}
	}
	
	public static void unDisguise(Player p) {
		if (Main.DisguiseCraftEnable){
			if (DisguiseData.isDisguised(p))
			dcAPI.undisguisePlayer(p);
			if (DisguiseData.PlayerDisguises.containsKey(p.getName()))
				DisguiseData.PlayerDisguises.remove(p.getName());
			
			if (Main.Tasks.containsKey(p.getName())){
				Main.Tasks.get(p.getName()).cancel();
				Main.Tasks.remove(p.getName());
			}
			if (Main.CountdownTimer.containsKey(p.getName()))
			Main.CountdownTimer.remove(p.getName());		
		}
	}
	
	
    public static DisguiseType getDisguiseType(EntityType entity) {
        
        if (entity == EntityType.BLAZE) {
                return DisguiseType.Blaze;
        }
        else if (entity == EntityType.CAVE_SPIDER) {
                return DisguiseType.CaveSpider;
        }
        else if (entity == EntityType.CHICKEN) {
                return DisguiseType.Chicken;
        }
        else if (entity == EntityType.COW) {
                return DisguiseType.Cow;
        }
        else if (entity == EntityType.CREEPER) {
                return DisguiseType.Creeper;
        }
        else if (entity == EntityType.ENDER_DRAGON) {
                return DisguiseType.EnderDragon;
        }
        else if (entity == EntityType.ENDERMAN) {
                return DisguiseType.Enderman;
        }
        else if (entity == EntityType.GHAST) {
                return DisguiseType.Ghast;
        }
        else if (entity == EntityType.GIANT) {
                return DisguiseType.Giant;
        }
        else if (entity == EntityType.IRON_GOLEM) {
                return DisguiseType.IronGolem;
        }
        else if (entity == EntityType.MAGMA_CUBE) {
                return DisguiseType.MagmaCube;
        }
        else if (entity == EntityType.MUSHROOM_COW) {
                return DisguiseType.MushroomCow;
        }
        else if (entity == EntityType.OCELOT) {
                return DisguiseType.Ocelot;
        }
        else if (entity == EntityType.PIG) {
                return DisguiseType.Pig;
        }
        else if (entity == EntityType.PIG_ZOMBIE) {
                return DisguiseType.PigZombie;
        }
        else if (entity == EntityType.SHEEP) {
                return DisguiseType.Sheep;
        }
        else if (entity == EntityType.SILVERFISH) {
                return DisguiseType.Silverfish;
        }
        else if (entity == EntityType.SKELETON) {
                return DisguiseType.Skeleton;
        }
        else if (entity == EntityType.SLIME) {
                return DisguiseType.Slime;
        }
        else if (entity == EntityType.SNOWMAN) {
                return DisguiseType.Snowman;
        }
        else if (entity == EntityType.SPIDER) {
                return DisguiseType.Spider;
        }
        else if (entity == EntityType.SQUID) {
                return DisguiseType.Squid;
        }
        else if (entity == EntityType.VILLAGER) {
                return DisguiseType.Villager;
        }
        else if (entity == EntityType.WOLF) {
                return DisguiseType.Wolf;
        }
        else if (entity == EntityType.ZOMBIE) {
                return DisguiseType.Zombie;
        }
        else if (entity == EntityType.WITCH) {
                return DisguiseType.Witch;
        }
        else if (entity == EntityType.BAT) {
                return DisguiseType.Bat;
        }
        else if (entity == EntityType.WITHER) {
                return DisguiseType.Wither;
        }
        return null;
    }


	public static boolean isDisguised(Player p) {
		return dcAPI.isDisguised(p);
	}
    
}
