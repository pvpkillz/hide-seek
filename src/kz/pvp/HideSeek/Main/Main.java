package kz.pvp.HideSeek.Main;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Logger;

import kz.pvp.HideSeek.Commands.GameCommands;
import kz.pvp.HideSeek.Commands.PlayerCommands;
import kz.pvp.HideSeek.Enums.GameState;
import kz.pvp.HideSeek.Events.BlockEverything;
import kz.pvp.HideSeek.Events.DeathListener;
import kz.pvp.HideSeek.Events.InteractListener;
import kz.pvp.HideSeek.Events.JoinListener;
import kz.pvp.HideSeek.Events.MoveListener;
import kz.pvp.HideSeek.Methods.ConvertTimings;
import kz.pvp.HideSeek.Methods.DisguiseData;
import kz.pvp.HideSeek.Methods.Disguises;
import kz.pvp.HideSeek.Methods.FakeShow;
import kz.pvp.HideSeek.Methods.FileManagement;
import kz.pvp.HideSeek.Methods.ProtocolLib;
import kz.pvp.HideSeek.Methods.TeamManagement;
import kz.pvp.HideSeek.Methods.TimedCommand;
import kz.pvp.HideSeek.Timers.BeginGameTimer;
import kz.pvp.HideSeek.Timers.EndGameTimer;
import kz.pvp.HideSeek.Timers.HideTimer;
import kz.pvp.HideSeek.Timers.UpdateSigns;
import kz.pvp.HideSeek.Utilities.Files;
import kz.pvp.HideSeek.Utilities.Message;
import me.confuser.barapi.BarAPI;

import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;


public class Main extends JavaPlugin{
	public static boolean DisguiseCraftEnable = false;// Is Disguisecraft enabled?
	public static boolean ProtocolLibEnable = false;// Is ProtocolLib enabled?
	public static String DBHost = null;
	public static String DBName = null;
	public static String DBPass = null;
	public static String DBUser = null;
	public static String Map = "";
	
	private void setupConfigValues(FileConfiguration conf) {
		String confPath = "";
		
		
		confPath = "Mysql.Enable";
		if (conf.contains(confPath))
			UseMySQL = conf.getBoolean(confPath);
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
		
	}
	public static HashMap<String, BukkitTask> Tasks = new HashMap<String, BukkitTask>();
	public static HashMap<String, Integer> CountdownTimer = new HashMap<String, Integer>();

	
	
	
	
	public static ArrayList<Player> Editor = new ArrayList<Player>();
	public static int CurrentSeconds = 0;
	
	
	
	public static GameState game = GameState.PREGAME;

	Logger log = Logger.getLogger("Minecraft");
	
	
	// Variable declarations below
	public static Main plugin;
	public void onEnable()
    {
		EnableEvents();

		for (Player pl : getServer().getOnlinePlayers()){
			pl.teleport(pl.getWorld().getSpawnLocation());
			JoinListener.JoinedServer(pl);
		}
		
		
		
		
		
		
		
		
		
		
		log.info("Enabling " + this.getDescription().getName() + " Version " + this.getDescription().getVersion() + " by Undeadkillz");
    }
	public void onDisable()
    {
		log.info("Disabling " + this.getDescription().getName() + " Version " + this.getDescription().getVersion() + " by Undeadkillz");
    }
	
	
	public static boolean UseMySQL;
	private void EnableEvents() {
		// Load Config
		new Files(this);
	  	/*
	  	 * Utilities
	  	 */
	  	new Message(this);
	  	new FileManagement(this);
	  	/*
	  	 * Main Classes
	  	 */
	  	new HideSeek(this);
	  	new Mysql(this);
	  	new TeamManagement(this);
	  	new ConvertTimings(this);
	  	new TimedCommand(this);
	  	new BlockEverything(this);
	  	new MoveListener(this);
	  	new DeathListener(this);
	  	new InteractListener(this);
	  	new JoinListener(this);
	  	new BeginGameTimer(this);
	  	new EndGameTimer(this);
	  	new UpdateSigns(this);
	  	new HideTimer(this);
	  	///////////////////////////////
	  	if (Files.arenas.contains("Maps")){
	  	List<String> maps = Files.arenas.getStringList("Maps");
	  	
	  	
		Random r = new Random();
		int Low = 0;
		int High = maps.size();
		int R = r.nextInt(High-Low) + Low;
		String map = maps.get(R);
	  	Map = map;
	  	HideSeek.MOTD = "Open | " + Map;
	  	
	  	
	  	BukkitTask Timer = new BeginGameTimer(this).runTaskTimer(this, 20L, 20L);
	  	BeginGameTimer.Timer = Timer;
	  	BukkitTask SignUpdate = new UpdateSigns(this).runTaskTimer(this, 20L, 20L);
	  	UpdateSigns.Task = SignUpdate;
	  	
	  	
	  	
	  	
    	this.getServer().getScheduler().runTaskTimerAsynchronously(this, new Runnable() {
    	    @Override  
    	    public void run() {
    	        for (Player pl : getServer().getOnlinePlayers()){
    	    		BarAPI.setMessage(pl, Message.ClearUp("&a" + Message.CurrentTime));
    	        }
    	    }
    	}, 10L, 10L);
	  	

	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	
	  	}
	  	
    	FileConfiguration conf = Files.config;
    	setupConfigValues(conf);
		if (UseMySQL == true){
			DBHost = conf.getString("Mysql.Host");
			DBName = conf.getString("Mysql.TableName");
			DBUser = conf.getString("Mysql.User");
			DBPass = conf.getString("Mysql.Password");
			Mysql.ConnectToDB();
		}
		
		
		Plugin protocolLib = this.getServer().getPluginManager().getPlugin("ProtocolLib");
		Plugin disguiseCraft = this.getServer().getPluginManager().getPlugin("DisguiseCraft");

		if (protocolLib == null)
			Main.ProtocolLibEnable = false;
		else{
			Main.ProtocolLibEnable = true;
			new ProtocolLib(this);
		}
		
		if (disguiseCraft == null || protocolLib == null)
			Main.DisguiseCraftEnable = false;
		else{
			Main.DisguiseCraftEnable = true;
			new Disguises(this);
			new DisguiseData(this);
			new FakeShow(this);
		}
		
	  	RegisterAllCommands();// Register all commands for this plugin
	}
	
	
	private void RegisterAllCommands() {
    	getCommand("hideseek").setExecutor(new GameCommands(this));// Main Hide N Seek Admin command
    	getCommand("hs").setExecutor(new GameCommands(this));// Main Hide N Seek Admin command
    	getCommand("gems").setExecutor(new PlayerCommands(this));// Main Hide N Seek Gem command

	}
	
	
	
}
