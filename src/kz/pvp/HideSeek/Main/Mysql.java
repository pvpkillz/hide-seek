package kz.pvp.HideSeek.Main;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;



public class Mysql {
	
	static Main plugin;
	static Connection con = null;
	
	public Mysql(Main mainclass) {
		plugin = mainclass;
	}
	
	
	public static int GetUserID (String p) throws SQLException{
		ResultSet res = con.createStatement().executeQuery("SELECT * FROM Users WHERE LOWER(Username) = LOWER('" + p + "') LIMIT 1");
		int userid;
		if (res.next())
		userid = Integer.parseInt(res.getString("ID"));
		else
		userid = 0;
		
		return userid;
	}
	public static String getUserName(int id) throws SQLException {
		ResultSet res = con.createStatement().executeQuery("SELECT * FROM Users WHERE ID = '" + id + "' LIMIT 1");
		String username;
		if (res.next())
		username = res.getString("Username");
		else
		username = "None";
		
		return username;
	}
	
	public static PreparedStatement PS (String query) throws SQLException{
		// We will make sure our database is safe from injections.
		PreparedStatement ps = con.prepareStatement(query);
		return ps;
	}
	
	public static ResultSet dbGet(String query) throws SQLException{
		ResultSet rs = null;
		try {
			rs = con.createStatement().executeQuery(query);
		} catch (SQLException e) {
			System.out.println("Failed to get info from Database. Make sure connection is open!");
		}
		return rs;
	}
	
	public static int getPlayerGems(String name) throws SQLException {
		ResultSet res = con.createStatement().executeQuery("SELECT * FROM Global_Gems WHERE ID = '" + GetUserID(name) + "' LIMIT 1");
		int userpts;
		if (res.next())
		userpts = Integer.parseInt(res.getString("Gems"));
		else{
		con.createStatement().executeUpdate("INSERT INTO Global_Gems (ID, Gems) VALUES ('" + GetUserID(name) + "','0')");
		userpts = 0;
		}
		return userpts;
	}
	
	
	
	public static boolean modifyUserGems (String p, int Change) throws SQLException{
		int Player = GetUserID (p);
		
		int curPts = getPlayerGems(p);
		Player user = Bukkit.getPlayerExact(p);

		if (Change < 0){// We want to take away coins
			if(curPts < Math.abs(Change)){// Not enough coins
				Message.P(user, Message.Replacer(Message.Replacer(Message.CannotAfford, "" + curPts, "%gems"), "" + Math.abs(Change), "%cost"), true);
				return false;
			}
			else if (curPts >= Math.abs(Change)){// Enough coins met
				int newPts = curPts - Math.abs(Change);
				Message.P(user, Message.Replacer(Message.Replacer(Message.PurchaseSuccess, "" + newPts, "%gems"), "" + Math.abs(Change), "%cost"), true);
				
				PreparedStatement ps = Mysql.PS("UPDATE Global_Gems SET Gems = ? WHERE ID = ?");
				ps.setInt(1, newPts);
				ps.setInt(2, Player);
				ps.executeUpdate();
				//dbSubmit("UPDATE Points SET Points = '" + newPts + "' WHERE ID = '" + Player + "'");
				
				return true;
			}
			
		}
		else if (Change >= 0){// We want to give coins
			int newPts = curPts + Math.abs(Change);
			//dbSubmit("UPDATE Points SET Points = '" + newPts + "' WHERE ID = '" + Player + "'");
			Message.P(user, Message.Replacer(Message.Earned, "" + Change, "%earn"), true);
			PreparedStatement ps = Mysql.PS("UPDATE Global_Gems SET Gems = ? WHERE ID = ?");
			ps.setInt(1, newPts);
			ps.setInt(2, Player);
			ps.executeUpdate();
			
			return true;
		}
		return false;
	}


	public static void ConnectToDB() {
		try
		{
		  String myDriver = "com.mysql.jdbc.Driver";
		  String myUrl = "jdbc:mysql://" + Main.DBHost + "/" + Main.DBName;
		  Main.UseMySQL = true;
		  Class.forName(myDriver);
		  // We connected, so we don't have to ever close it! :D
		  con = DriverManager.getConnection(myUrl, Main.DBUser, Main.DBPass);
		  System.out.println("Successfully connected to the Database for " + plugin.getName());
		}
		catch (Exception e)
		{
			
			System.err.println("Failed to connect to Database!");
			Main.UseMySQL = false;
			System.err.println(e.getMessage());
			
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					ConnectToDB();// We will schedule a delayed task to reconnect to DB when possible.
				}
			}, 20L * 60);
		
		
		}
	}


	public static void CheckUserStatus(Player p) throws SQLException {
		if (Mysql.GetUserID(p.getName()) == 0){
		PreparedStatement ps = Mysql.PS("INSERT INTO Users (Username) VALUES (?)");
		ps.setString(1, p.getName());
		ps.executeUpdate();
		
		PreparedStatement ps2 = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
		ps2.setInt(1, Mysql.GetUserID(p.getName()));
		ps2.setLong(2, System.currentTimeMillis()/1000);
		ps2.setString(3, p.getAddress().getAddress().getHostAddress());
		ps2.executeUpdate();
		
		
		
		Message.P(p, ChatColor.GOLD + "Welcome first timer, " + p.getName() + "!", false);
		}
		
		
		
		
		ResultSet rss = Mysql.dbGet("SELECT * FROM UserInfo WHERE User = '" + Mysql.GetUserID(p.getName()) + "'");
				
		if (!rss.next()){
			PreparedStatement ps = Mysql.PS("INSERT INTO UserInfo (User, JoinDate, LastLogin) VALUES (?,?,?)");
			ps.setInt(1, Mysql.GetUserID(p.getName()));
			ps.setLong(2, System.currentTimeMillis()/1000);
			ps.setString(3, p.getAddress().getAddress().getHostAddress());
			ps.executeUpdate();
		}
	}
	public static Player getPlayerFromSome(String partname) {
		Player target = null;
		for (Player p : plugin.getServer().getOnlinePlayers()){
			if (p.getName().equalsIgnoreCase(partname) || p.getName().toLowerCase().contains(partname.toLowerCase())){
				target = p;
				break;// We found our match! :P
			}
		}
		return target;
	}
}
