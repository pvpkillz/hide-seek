package kz.pvp.HideSeek.Main;

import java.sql.SQLException;
import java.util.ArrayList;

import kz.pvp.HideSeek.Enums.Team;
import kz.pvp.HideSeek.Methods.ConvertTimings;
import kz.pvp.HideSeek.Methods.ProtocolLib;
import kz.pvp.HideSeek.Methods.TeamManagement;
import kz.pvp.HideSeek.Timers.HideTimer;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.block.Block;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.server.ServerListPingEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitTask;



public class HideSeek extends JavaPlugin implements Listener{

	public static Main plugin;
	public HideSeek(Main mainclass) {
		plugin = mainclass;
		mainclass.getServer().getPluginManager().registerEvents(this, mainclass);
	}
	
	public static int StartTime = 300;//Start timer to begin game!
	public static final int EndGameTimer = 60 * 10;// Time for round end
	public static final int HideTime = 30;// Time for hiding
	public static final int MinStart = 2;// Players needed to start
	public static final int QuickStart = 12;// Players needed for quick start
	public static String MOTD = "Open | " + Main.Map;
	//Location hiders = FileManagement.GetLocOfString("Map." + Main.Map + ".Hiders", Files.arenas);

	
	
	public static void BeginGame(){
		MOTD = "In-Progress | " + Main.Map;
		
		plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
			public void run() {
				for (Player pl: TeamManagement.Hiders){
					
					ItemStack IS = new ItemStack(Material.WOOD_SWORD, 1);
					IS.addEnchantment(Enchantment.KNOCKBACK, 1);
					Message.P(pl, Message.GivenASword, true);
					pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 2f, 1f);
					pl.getInventory().addItem(IS);
				}
			}
		}, 20L * 60 * 3);
		
		
		ArrayList<Player> Players = new ArrayList<Player>();

		for (Player pl : plugin.getServer().getOnlinePlayers()){
				Players.add(pl);
			}
		
		if (TeamManagement.getSeekersSize() == 0){
			int Random = ConvertTimings.randomInt(0, Players.size() - 1);
			Player p = Players.get(Random);
			TeamManagement.joinSeekers(p, HideSeek.HideTime);
			Players.remove(p);
		}
		
		
		for (Player pl : Players){
			TeamManagement.joinHiders(pl);
		}
		

	  	BukkitTask Timer = new HideTimer(plugin).runTaskTimer(plugin, 20L, 20L);
	  	HideTimer.Timer = Timer;
		
		
		
		
		
	}
	
	
	
	
	
	@EventHandler
	public void onPing (ServerListPingEvent e){
		e.setMotd(MOTD);
	}
	
	public static void GiveItem(Player p, String Name, Material ItemMaterial, int ItemAmount, int slot, String Lore, boolean Glow) {
		ItemStack Item = new ItemStack(ItemMaterial, ItemAmount);
		
		
		ItemMeta im = Item.getItemMeta();
		if (Name != null)
			im.setDisplayName(Name);
		if (Lore != null)
			im.getLore().add(Lore);
		
		if (Glow == true && Main.ProtocolLibEnable == true)
		Item = ProtocolLib.setGlowing(Item);
		
		p.getInventory().setItem(slot, Item);
	}
	
	public static boolean IsSign(Block b) {
		if (b.getType() == Material.WALL_SIGN || b.getType() == Material.SIGN || b.getType() == Material.SIGN_POST)
		return true;
		else
		return false;
	}





	public static void EndGame(Team team) {
		if (team == Team.HIDERS){
			Message.G(Message.HidersWon, true);
			try {
				for (Player hider : TeamManagement.Hiders)
				Mysql.modifyUserGems(hider.getName(), 5);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					plugin.getServer().shutdown();
				}
			}, 20L * 5);
		}
		else{
			Message.G(Message.SeekersWon, true);
			plugin.getServer().getScheduler().scheduleSyncDelayedTask(plugin, new Runnable() {
				public void run() {
					plugin.getServer().shutdown();
				}
			}, 20L * 5);
		}
		
	}





	public static void checkStatus() {
		if (TeamManagement.getSeekersSize() == 0){
			PickNewSeeker();
			
		}
		else if (TeamManagement.getHidersSize() == 0)
			EndGame(Team.SEEKERS);
	}





	private static void PickNewSeeker() {
		TeamManagement.joinSeekers(TeamManagement.Hiders.get(ConvertTimings.randomInt(0, TeamManagement.getHidersSize() - 1)), 0);
		
		
		
	}
	
}
