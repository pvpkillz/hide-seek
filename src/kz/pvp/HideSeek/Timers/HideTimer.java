 package kz.pvp.HideSeek.Timers;

import kz.pvp.HideSeek.Enums.GameState;
import kz.pvp.HideSeek.Events.JoinListener;
import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;




public class HideTimer extends BukkitRunnable {

	static Main plugin;
	public static BukkitTask Timer;// The repeating task's ID
	
    public HideTimer(Main main) {
        plugin = main;
        Main.CurrentSeconds = HideSeek.HideTime;
    }

	@Override
	public void run() {
	    	if (Main.CurrentSeconds >= 0){
	    		Main.CurrentSeconds--;
	    		for (Player p : plugin.getServer().getOnlinePlayers()){
	    			p.setHealth(20.0);
	    			p.setLevel(Main.CurrentSeconds);
	    			p.setFoodLevel(20);
	    			JoinListener.ScoreBoard(p);
	    		}
	    		
	    		
	    		if (Main.CurrentSeconds <= 0){
	    			Message.G(Message.SeekerReleased, true);
	    			Main.game = GameState.GAME;
	    		  	BukkitTask Timed = new EndGameTimer(plugin).runTaskTimer(plugin, 20L, 20L);
	    		  	EndGameTimer.Timer = Timed;
	    		  	
	    			Timer.cancel();
	    		}
	    		
	    		
	    		
	    		
	    		
	    	}
	}
}
