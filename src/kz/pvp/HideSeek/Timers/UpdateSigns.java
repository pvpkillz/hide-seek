package kz.pvp.HideSeek.Timers;

import kz.pvp.HideSeek.Enums.GameState;
import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Files;

import org.bukkit.ChatColor;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.block.Sign;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;





public class UpdateSigns extends BukkitRunnable {

	static Main plugin;
	static FileConfiguration var;
	public static BukkitTask Task;
    public UpdateSigns(Main main) {
        plugin = main;
		var = Files.signs;
    }

	@Override
	public void run() {
		if (Main.game == GameState.GAME){
			Task.cancel();
		}
		if (var.contains("signs.players"))
		for ( String  f : var.getConfigurationSection("signs.players").getKeys(false)){
			double X = (double) var.get("signs.players." + f + ".X");
			double Y = (double) var.get("signs.players." + f + ".Y");
			double Z = (double) var.get("signs.players." + f + ".Z");
			String World = (String) var.get("signs.players." + f + ".World");
			World w = plugin.getServer().getWorld(World);
			Block b = w.getBlockAt((int) X, (int) Y, (int)Z);
			if (HideSeek.IsSign(b)){
				Sign s = (Sign) b.getState();
				s.setLine(0, ChatColor.DARK_RED + "Players");
				s.setLine(1, ChatColor.DARK_BLUE  + "" + plugin.getServer().getOnlinePlayers().length);
				s.setLine(2, ChatColor.DARK_RED + "out of");
				s.setLine(3, ChatColor.DARK_BLUE + "" + plugin.getServer().getMaxPlayers());
				s.update();
			}
		}
		
		if (var.contains("signs.map"))
		for ( String  f : var.getConfigurationSection("signs.map").getKeys(false)){
			double X = (double) var.get("signs.map." + f + ".X");
			double Y = (double) var.get("signs.map." + f + ".Y");
			double Z = (double) var.get("signs.map." + f + ".Z");
			String World = (String) var.get("signs.map." + f + ".World");
			World w = plugin.getServer().getWorld(World);
			Block b = w.getBlockAt((int) X, (int) Y, (int)Z);
			if (HideSeek.IsSign(b)){
				Sign s = (Sign) b.getState();
				s.setLine(0, ChatColor.BLUE + "Map");
				s.setLine(1, ChatColor.BLACK  + "" + Main.Map);
				s.setLine(2, ChatColor.GOLD + "Round Length");
				s.setLine(3, ChatColor.BLACK + "" + HideSeek.EndGameTimer + " seconds.");
				s.update();
			}
		}
		if (var.contains("signs.start"))
		for ( String  f : var.getConfigurationSection("signs.start").getKeys(false)){
			double X = (double) var.get("signs.start." + f + ".X");
			double Y = (double) var.get("signs.start." + f + ".Y");
			double Z = (double) var.get("signs.start." + f + ".Z");
			String World = (String) var.get("signs.start." + f + ".World");
			World w = plugin.getServer().getWorld(World);
			Block b = w.getBlockAt((int) X, (int) Y, (int)Z);
			if (HideSeek.IsSign(b)){
				Sign s = (Sign) b.getState();
				s.setLine(0, ChatColor.DARK_RED + "Starting in");
				s.setLine(1, ChatColor.BLACK  + "" + Main.CurrentSeconds);
				s.update();
			}
		}
		
		
		
	}
}
