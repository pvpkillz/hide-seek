package kz.pvp.HideSeek.Timers;

import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;





public class BeginGameTimer extends BukkitRunnable {

	static Main plugin;
	public static BukkitTask Timer;// The repeating task's ID
	
    public BeginGameTimer(Main main) {
        plugin = main;
        Main.CurrentSeconds = HideSeek.StartTime;
    }

	@Override
	public void run() {
	    	if (Main.CurrentSeconds >= 0){
	    		Main.CurrentSeconds--;
	    		
	    		for (Player p : plugin.getServer().getOnlinePlayers()){
	    			p.setHealth(20.0);
	    			p.setLevel(Main.CurrentSeconds);
	    			p.setFoodLevel(20);
	    		}
	    		if (Main.CurrentSeconds <= 0){
	    			if (plugin.getServer().getOnlinePlayers().length >= HideSeek.MinStart){
	    			HideSeek.BeginGame();
	    			Timer.cancel();
	    			}
	    			else{
	    				Main.CurrentSeconds = HideSeek.StartTime;
	    			}
	    		}
	    		else if (Main.CurrentSeconds <= 10){
	    			for (Player p : plugin.getServer().getOnlinePlayers()){
	    				p.playSound(p.getLocation(), Sound.CLICK, 2f, 1f);
	    			}
	    			if (Main.CurrentSeconds == 10){
	    				 Message.G(Message.GameBegins, false);
	    				 Message.G(Message.MapPlayed, false);
	    				 Message.G(Message.PlayersOn, false);
	    				 }
	    			}

	    	}
	}
}
