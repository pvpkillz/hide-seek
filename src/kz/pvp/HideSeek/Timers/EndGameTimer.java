package kz.pvp.HideSeek.Timers;

import kz.pvp.HideSeek.Enums.Team;
import kz.pvp.HideSeek.Events.JoinListener;
import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;





public class EndGameTimer extends BukkitRunnable {

	static Main plugin;
	static BukkitTask Timer;// The repeating task's ID
    public EndGameTimer(Main main) {
        plugin = main;
        Main.CurrentSeconds = HideSeek.EndGameTimer;
    }

	@Override
	public void run() {
		
	    if (Main.CurrentSeconds >= 0){
	    		
	    	for (Player p : plugin.getServer().getOnlinePlayers()){
	    		p.setFoodLevel(20);
	    		JoinListener.ScoreBoard(p);
	    	}
	    		
	    	
	    	Main.CurrentSeconds--;
	    
	    	if (Main.CurrentSeconds <= 0){
	    		for (Player pl : plugin.getServer().getOnlinePlayers())
	    			pl.playSound(pl.getLocation(), Sound.LEVEL_UP, 2f, 1f);
	    		HideSeek.EndGame(Team.HIDERS);
	    		Timer.cancel();
	    	}
	    }
	}
}
