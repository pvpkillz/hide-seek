package kz.pvp.HideSeek.Timers;

import kz.pvp.HideSeek.Main.HideSeek;
import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.DisguiseData;
import kz.pvp.HideSeek.Methods.Disguises;
import kz.pvp.HideSeek.Utilities.Message;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;





public class FreezeTimer extends BukkitRunnable {

	static Main plugin;
	Player p;
	public static BukkitTask Timer;
    public FreezeTimer(Main main, Player pl) {
        plugin = main;
        Timer = null;
        p = pl;
    }

	@SuppressWarnings("deprecation")
	@Override
	public void run() {
		int CurrentTime = Main.CountdownTimer.get(p.getName());
		
	    	if (CurrentTime >= 0){
	    		Main.CountdownTimer.put(p.getName(), CurrentTime - 1);
	    		CurrentTime = Main.CountdownTimer.get(p.getName());
	    		
	    		if (CurrentTime <= 0){
	    			if (!DisguiseData.isFrozenDisguised(p)){
		    			Block block = p.getWorld().getBlockAt(p.getLocation());
		    			if (block.getTypeId() == 0 || block.isLiquid()){
				    		Disguises.disguiseAsB(p, DisguiseData.getBlockDisguise(p), true);
		    				HideSeek.GiveItem(p, "Active - My disguise", DisguiseData.getBlockDisguise(p).getType(), 1, 8, null, true);
		    			}
		    			else
		    				Message.P(p, Message.InvalidLoc, true);
	    			}
	    		}
	    		else{
	    			HideSeek.GiveItem(p, "My disguise", DisguiseData.getBlockDisguise(p).getType(), CurrentTime, 8, null, false);
	    		}
	    	}
	}
}
