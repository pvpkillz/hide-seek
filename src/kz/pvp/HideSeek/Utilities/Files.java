package kz.pvp.HideSeek.Utilities;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;



import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;


public class Files{
	public static Main plugin;
	static Logger log = Bukkit.getLogger();
	public static ArrayList<String> ConfigFiles = new ArrayList<String>();

	public Files (Main mainclass){
		plugin = mainclass;
		ConfigFiles.add("config.yml");
		ConfigFiles.add("signs.yml");
		ConfigFiles.add("arenas.yml");
		try{
			loadFiles();
		}catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static FileConfiguration signs;
	public static FileConfiguration arenas;
	public static FileConfiguration config;

	public static void copy(InputStream in, File file) {
		try {
			OutputStream out = new FileOutputStream(file);
			byte[] buf = new byte[1024];
			int len;
			while ((len = in.read(buf)) > 0) {
				out.write(buf, 0, len);
			}
			out.close();
			in.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	
	public static void loadFiles() throws Exception {
		// We di not need the extra code... We will just use an arraylist
		for (String file : ConfigFiles){
			File workingFile = new File(plugin.getDataFolder(), file);
			
			if (!workingFile.exists()) {
				workingFile.getParentFile().mkdirs();
				Files.copy(plugin.getResource(file), workingFile);
			}
		}
		
		
		
		config = YamlConfiguration.loadConfiguration(
				new File(plugin.getDataFolder(), "config.yml"));
		signs = YamlConfiguration.loadConfiguration(
				new File(plugin.getDataFolder(), "signs.yml"));
		arenas = YamlConfiguration.loadConfiguration(
				new File(plugin.getDataFolder(), "arenas.yml"));
	}


	public static void saveFile(FileConfiguration file, String fileName) {
		try {
			file.save(new File(plugin.getDataFolder(), fileName));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
