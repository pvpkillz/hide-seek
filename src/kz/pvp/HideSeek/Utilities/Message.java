package kz.pvp.HideSeek.Utilities;

import java.util.logging.Logger;

import kz.pvp.HideSeek.Main.Main;
import kz.pvp.HideSeek.Methods.ConvertTimings;
import kz.pvp.HideSeek.Methods.TeamManagement;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;


public class Message extends JavaPlugin{
	public static Main plugin;
	public static String PREFIX = ChatColor.GOLD + "[" + ChatColor.RED + "H&S" + ChatColor.GOLD + "] " + ChatColor.GREEN;
	/*
	 *  Translation file (Will be extra, since this plugin will be used in english, but I need a challenge)
	 */
	
	public static String GameBegun = "The game has begun.";
	public static final String Selected = "You have selected point #<#>";
	public static final String YouAreEditing = "You have enabled editing mode.";
	public static final String YouAreNotEditing = "You have disabled editing mode.";
	
	
	public static final String Day = "day";
	public static final String Days = "days";
	public static final String Hours = "hours";
	public static final String Hour = "hour";
	public static final String Minutes = "minutes";
	public static final String Minute = "minute";	
	public static final String Second = "second";	
	public static final String Seconds = "seconds";
	public static final String HaveTimeLeft = "&7You have &5%time&7 to enter the command.";
	public static final String SeekerReleased = "&aThe seeker is now free!";
	public static final String GameBegins = "&7The game will begin in &c%time";
	public static final String MapPlayed = "&7The following map will be played: &a%map";
	public static final String PlayersOn = "&7There are now &a%online&7 players on.";
	public static final String HiderEliminated = "&7Hider &9%hider&7 has &cdied&7....";// NEED FIX
	public static final String HiddenAs = "&7You are now a &6%block block.";
	public static final String YouEliminated = "You have eliminated &9%hider&a.";
	public static final String HidersRemain = "&7%hiders &8remain in the game.";
	public static final String JoinedServer = "%player has joined the server.";
	public static final String GameInProgress = "&cGame is currently in progress.";
	public static final String SeekersWon = "&aSeekers have won. &7With no hiders remaining.";
	public static final String HidersWon = "&aHiders have won! &7With %hiders remaining.";
	public static final String GivenASword = "&bYou have been given a wooden sword.";
	public static final String CannotAfford = "&cYou are unable to do pay.&7 You have only &a%gems&7 and need &c%cost";
	public static final String PurchaseSuccess = "&7You now have &a%gems&7 after paying &c%cost&7.";
	public static final String Earned = "You have earned %earn gems.";
	public static final String InvalidLoc = "&cInvalid location. &aChoose an empty location.";
	public static final String SolidBlock = "&7You are now a &6solid %disguise&7.";
	public static final String YourGems = "Your Gems";
	public static final String Title = "&8&m---------&r &a%title &8&m---------";
	public static final String PlayerGems = "%player Gems";
	public static final String InvalidPlayer = "&cError: &7Player not online";
	public static final String CurrentTime = "Current time left: %time";
	public static final String CurrentTimeSet = "The time has been set.";
	public static final String InvalidEntry = "Your arguments were invalid.";
	
	public Message (Main mainclass){
		plugin = mainclass;
	}
	
	
	
	
	static Logger log = Bukkit.getLogger();
	
	public static void G (String Message, boolean Prefix){
		Message = ClearUp(Message);
		
		if (Prefix == true)
		plugin.getServer().broadcastMessage(PREFIX + ChatColor.GRAY + Message);
		else
		plugin.getServer().broadcastMessage(ChatColor.GREEN + Message);
	}
	
	
	
	public static String ClearUp(String Msg) {
		Msg = Message.Replacer(Msg, "" + plugin.getServer().getOnlinePlayers().length, "%online");
		Msg = Message.Replacer(Msg, "" + TeamManagement.getHidersSize(), "%hiders");
		Msg = Message.Replacer(Msg, "" + TeamManagement.getSeekersSize(), "%seekers");
		Msg = Message.Replacer(Msg, "" + ConvertTimings.convertTime(Main.CurrentSeconds), "%time");
		Msg = Message.Replacer(Msg, "" + CleanCapitalize(Main.Map), "%map");
		Msg = ChatColor.translateAlternateColorCodes('&', Msg);
		return Msg;
	}
	
	
	
	
	public static void P (Player p, String Message, boolean Prefix){
		Message = ClearUp(Message);
		
		if (!Prefix)
		p.sendMessage(ChatColor.GREEN + Message);
		else
		p.sendMessage(PREFIX + Message);
		
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(Message) + " | sent to " + p.getName());
	}
	
	
	
	
	public static void NP (Player p, String Message, boolean Prefix){
		Message = ClearUp(Message);
		P(p, ChatColor.GREEN + Message, Prefix);
		log.info(ChatColor.stripColor(Message) + " | sent to " + p.getName());
	}
	
	
	public static void GPAll (Player p, String Message, boolean Prefix){
		Message = ClearUp(Message);

		for (Player pl : plugin.getServer().getOnlinePlayers()){
			if (pl != p)
			P(pl,ChatColor.GRAY + Message, Prefix);
		}
		log.info(ChatColor.stripColor(PREFIX) + ChatColor.stripColor(Message) + " | sent to all players.");
	}
	
	public static String Replacer(String fullstr, String replace, String find) {
		if (fullstr.toLowerCase().contains(find.toLowerCase()))
		replace = fullstr.replace(find, replace);
		else
		replace = fullstr;
		
		replace = ChatColor.translateAlternateColorCodes('&', replace);

	return replace;
	}
	public static String CleanCapitalize(String msg) {
		if (msg.contains("_"))
		msg = msg.replace('_', ' ');
		
		msg = msg.toLowerCase().substring(0, 1).toUpperCase() + msg.toLowerCase().substring(1);
		
	return msg;
	}
	
}
